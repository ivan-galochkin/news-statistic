package main

import (
	"github.com/joho/godotenv"
	"log"
	"newsmeter/api"
	"newsmeter/processing"
)

func loadEnvs() {
	if err := godotenv.Load(); err != nil {
		log.Println("No env find")
	}
}

func main() {
	loadEnvs()
	processing.Processing()
	api.RunServer()
}
