package api

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"newsmeter/processing"
	"strconv"
	"strings"
)

type ResponseNewsCount struct {
	Count []int `json:"count"`
}

func SendNewsCount(w http.ResponseWriter, r *http.Request) {
	text := strings.Join(r.URL.Query()["text"], "")
	count, _ := strconv.Atoi(strings.Join(r.URL.Query()["count"], ""))
	dateStr := strings.Join(r.URL.Query()["date"], "")
	res := processing.GetNewsCountPeriod("everything", text, dateStr, count)
	response, _ := json.Marshal(res)
	_, err := w.Write(response)
	if err != nil {
		log.Panicln(err)
	}
}

func RunServer() {
	router := mux.NewRouter()
	router.HandleFunc("/news", SendNewsCount)
	http.Handle("/", router)
	http.ListenAndServe(":8888", router)
}
