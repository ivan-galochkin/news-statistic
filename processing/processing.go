package processing

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func GetNewsCountPeriod(mode string, searchWord string, today string, count int) []int {
	newsCount := make([]int, count)

	var Date, err = time.Parse("2006-01-02", today)
	if err != nil {
		log.Panicln(err)
	}

	for i := 0; i < count; i += 1 {
		DateIterator := Date.AddDate(0, 0, i-count+1).Format("2006-01-02")
		newsCount[i] = GetAndDecode(mode, searchWord, DateIterator)
	}

	return newsCount

}

func GetAndDecode(mode string, searchWord string, date string) int {
	return DecodeJson(GetNewsJson(mode, searchWord, date, date)).Count
}

func GetNewsJson(mode string, searchWord string, from string, to string) *http.Response {
	apiKey := os.Getenv("apiKey")
	request := fmt.Sprintf("https://newsapi.org/v2/%s?q=%s&from=%s&to=%s&apiKey=%s", mode, searchWord, from, to, apiKey)
	resp, err := http.Get(request)
	if err != nil {
		log.Println(err)
	}
	return resp
}

func DecodeJson(resp *http.Response) *NewsResponseScheme {
	data := NewsResponseScheme{}
	json.NewDecoder(resp.Body).Decode(&data)
	return &data
}

func Processing() {

}
