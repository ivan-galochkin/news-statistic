package processing

type NewsResponseScheme struct {
	Status   string     `json:"status"`
	Count    int        `json:"totalResults"`
	Articles []Articles `json:"articles"`
}

type Articles struct {
	Author string `json:"author"`
}
